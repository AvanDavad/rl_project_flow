from config import *
from util import get_frames_labels, export_video, eye_distances, draw_labels
import numpy as np
import cv2 as cv
import os

def sideface_statistics(labels):
	assert len(labels.shape)==3, 'shape of labels must be like (n_frames, n_parts, dim)'
	n_frames, n_parts, dim = labels.shape
	std_distances = []
	for frame in range(n_frames):
		std_distances.append(sideface_statistics_single(labels[frame]))
	return np.array(std_distances)

def sideface_statistics_single(labels):
	assert len(labels.shape) == 2, 'shape of labels must be like (n_parts, dim)'
	difference = labels[:16, :] - labels[1:17, :]
	distances = np.sqrt(np.sum(difference**2, axis=1))
	mean_distance = np.mean(distances)
	distances /= mean_distance
	std_distances = np.std(distances)
	return std_distances





