import numpy as np
import cv2 as cv
import os
import matplotlib.pyplot as plt
from config import *

def get_length(video_directory):
	cap = cv.VideoCapture(video_directory+video_name)
	length = cap.get(7)
	return int(length)

def get_frames(video_directory, length=-1):
	if length<0:
		length = get_length(video_directory) 
	cap = cv.VideoCapture(video_directory+video_name)
	print video_directory+video_name
	ret, frame = cap.read()
	assert ret==True, 'Failed to read frame.'
	frames = frame[np.newaxis, :, :, :]
	for i in range(length-1):
		ret, frame = cap.read()
		frames = np.append(frames, frame[np.newaxis, :, :, :], axis=0)
	cap.release()
	return frames

def get_labels(video_directory, length=-1):
	if length<0:
		length = get_length(video_directory)
	labels = np.empty((length, n_parts, dim), dtype=np.float)
	for frame in range(length):
		file_name = video_directory+annot_dir_name+str(frame+1).zfill(6)+'.pts'
		with open(file_name) as f:
			f.readline(); f.readline(); f.readline()
			for part in range(n_parts):
				line = f.readline()
				x, y = line.split()
				labels[frame, part, 0] = float(x)
				labels[frame, part, 1] = float(y)
			f.close()
	return labels

def get_frames_labels(video_directory, length=-1):
	frames = get_frames(video_directory, length)
	labels = get_labels(video_directory, length)
	return frames, labels

def draw_labels(frames, labels, radius=5, color=0, thickness=-1):
	assert frames.shape[0] == labels.shape[0]
	n_frames = frames.shape[0]
	labeled_frames = np.copy(frames)
	for frame in range(n_frames):
		for part in range(n_parts):
			x = labels[frame, part, 0]
			y = labels[frame, part, 1]
			cv.circle(labeled_frames[frame], center=(int(x), int(y)), radius=radius, color=color, thickness=thickness)
	return labeled_frames

def draw_nums(frames, nums, x=40, y=40, color=(255, 255, 255)):
	assert len(frames.shape)==4
	assert len(nums.shape)==1
	assert nums.shape[0] == frames.shape[0]
	n_frames = frames.shape[0]
	fontFace = 1
	fontScale = 1.0
	thickness = 1

	for frame in range(n_frames):
		frames[frame] = cv.putText(frames[frame], '%.3f' % nums[frame], \
								   (x, y), fontFace, fontScale, color, thickness)
	return frames

def eye_distances(labels):
	assert len(labels.shape)==3
	x1_sqr = labels[:, 36, 0]**2
	x2_sqr = labels[:, 45, 0]**2
	y1_sqr = labels[:, 36, 1]**2
	y2_sqr = labels[:, 45, 1]**2
	inner = labels[:, 36, 0] * labels[:, 45, 0] + labels[:, 36, 1] * labels[:, 45, 1]
	distances = np.sqrt(x1_sqr + x2_sqr + y1_sqr + y2_sqr - 2*inner)
	return distances

def eye_dist(labels):
	assert len(labels.shape)==2
	dist = np.sqrt((labels[36, 0] - labels[45, 0])**2 + (labels[36, 1] - labels[45, 1])**2)
	return dist

def transform_single_frame(frame, center=(0,0), scale=1.0, rot=0, translate=(0,0)):
	rows, cols, channels = frame.shape
	t_frame = np.copy(frame)
	M_scale_rot = cv.getRotationMatrix2D(center, rot, scale)
	M_translate = np.float32([[1,0,translate[0]], [0,1,translate[1]]])
	for ch in range(channels):
		img = t_frame[:,:,ch]
		img = cv.warpAffine(img, M_scale_rot, (cols, rows))
		img = cv.warpAffine(img, M_translate, (cols, rows))
		t_frame[:,:,ch]=img
	return t_frame

def transform_labels(labels, center=(0,0), scale=1.0, rot=0, translate=(0,0)):
	center = np.array([center[0], center[1]])
	translate = np.array([translate[0], translate[1]])
	relative_labels = labels - center
	rot_radian = rot*np.pi/180.0
	R = np.array([[np.cos(rot_radian), -np.sin(rot_radian)], [np.sin(rot_radian), np.cos(rot_radian)]])
	t_labels = center + translate + scale * np.dot(relative_labels, R)
	return t_labels

def transform_frame_labels(frame, labels, center=(0,0), scale=1.0, rot=0, translate=(0,0)):
	t_frame = transform_single_frame(frame, center, scale, rot, translate)
	t_labels = transform_labels(labels, center, scale, rot, translate)
	return t_frame, t_labels

def export_video(frames, name='movie', fps=20.0):
	print 'export: ', name
	fourcc = cv.VideoWriter_fourcc('X', 'V', 'I', 'D')
	n_frames, height, width, channels = frames.shape
	out = cv.VideoWriter(name, fourcc, fps, (width, height))
	for frame in range(n_frames):
		out.write(frames[frame])
	out.release()

def create_transformed_frames(frames, n_out, center, scale_tup, rot_tup, translate_tup):
	n_frames, height, width, channels = frames.shape
	start_scale, end_scale = scale_tup
	start_rot, end_rot = rot_tup
	start_translate, end_translate = translate_tup

	out_frames = np.zeros((n_out, height, width, channels))
	for frame in range(n_out):
		num_tup = (n_frames, n_out)
		mask, scale, rot, translate = create_transformed_helper(num_tup, frame, scale_tup, rot_tup, translate_tup)
		image = frames[mask]
		out_frames[frame] = transform_single_frame(image, center, scale, rot, translate)
	return out_frames.astype(np.uint8)


def create_transformed_labels(labels, n_out, center, scale_tup, rot_tup, translate_tup):
	n_labels, n_parts, dim = labels.shape
	start_scale, end_scale = scale_tup
	start_rot, end_rot = rot_tup
	start_translate, end_translate = translate_tup

	out_labels = np.zeros((n_out, n_parts, dim))
	for frame in range(n_out):
		num_tup = (n_labels, n_out)
		mask, scale, rot, translate = create_transformed_helper(num_tup, frame, scale_tup, rot_tup, translate_tup)
		out_labels[frame] = transform_labels(labels[mask], center, scale, rot, translate)
	return out_labels

def create_transformed_helper(num_tup, index, scale_tup, rot_tup, translate_tup):
	n_orig, n_out = num_tup
	start_scale, end_scale = scale_tup
	start_rot, end_rot = rot_tup
	start_translate, end_translate = translate_tup

	t = np.linspace(0.0, 0.999, num=n_out)
	mask = int(np.floor(n_orig*t[index]))

	scale = convex_combination(start_scale, end_scale, t[index])
	rot = convex_combination(start_rot, end_rot, t[index])
	translate_x = convex_combination(start_translate[0], end_translate[0], t[index])
	translate_y = convex_combination(start_translate[1], end_translate[1], t[index])
	translate = (translate_x, translate_y)

	return (mask, scale, rot, translate)

def convex_combination(start, end, fraction):
	return start*(1.0-fraction)+end*fraction

def create_transformed(frames, labels, num_out, center, scale_tup, rot_tup, translate_tup):
	out_frames = create_transformed_frames(frames, num_out, center, scale_tup, rot_tup, translate_tup)
	out_labels = create_transformed_labels(labels, num_out, center, scale_tup, rot_tup, translate_tup)
	return out_frames, out_labels

def export_labels_single(labels, frame_index=1, folder=''):
	filename = folder+str(frame_index).zfill(6)+'.pts'
	f = open(filename, 'w')
	f.write('version: 1\n')
	f.write('n_points: '+str(n_parts)+'\n')
	f.write('{\n')
	for i in range(n_parts):
		str_x = "{:.3f}".format(labels[i, 0])
		str_y = "{:.3f}".format(labels[i, 1])
		f.write(str_x+' '+str_y+'\n')
	f.write('}')
	f.close()

def export_labels(labels, folder='./labels/'):
	n_labels = labels.shape[0]
	for l in range(n_labels):
		export_labels_single(labels[l], frame_index=l+1, folder=folder)
	np.save(open(folder+'annot.npy', 'w'), labels)
	
def sample_frames(frames, sample_every=3):
	n_frames = frames.shape[0]
	n_sampled_frames = n_frames / sample_every
	sampled_frames = frames[np.arange(n_sampled_frames)*sample_every]
	return sampled_frames

def sample_labels(labels, sample_every=3):
	n_labels = labels.shape[0]
	n_sampled_labels = n_labels / sample_every
	sampled_labels = labels[np.arange(n_sampled_labels)*sample_every]
	return sampled_labels

def sample_frames_labels(frames, labels, sample_every=3):
	sampled_frames = sample_frames(frames, sample_every)
	sampled_labels = sample_labels(labels, sample_every)
	return sampled_frames, sampled_labels

def figure_to_image(figure):
	figure.canvas.draw()
	w,h=figure.canvas.get_width_height()
	buf = np.fromstring( figure.canvas.tostring_argb(), dtype=np.uint8)
	buf.shape=(h, w, 4) # Alpha-Red-Green-Blue
	image = buf[:, :, [1, 2, 3]] # discard alpha channel
	return image
	





