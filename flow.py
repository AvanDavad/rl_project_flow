import cv2 as cv
import os
import numpy as np

from util import *

lk_params = dict( winSize=(15,15), maxLevel=2, criteria=(cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))


def make_gray(frames):
	n_frames, height, width, channels = frames.shape
	gray_frames = np.zeros((n_frames, height, width))
	for frame in range(n_frames):
		gray_frames[frame] = cv.cvtColor(frames[frame], cv.COLOR_BGR2GRAY)
	gray_frames = gray_frames.astype(np.uint8)
	return gray_frames

def calc_flow_LK(frames, start_labels):
	assert len(frames.shape)==4
	assert len(start_labels.shape)==2
	
	n_frames, height, width, channels = frames.shape
	gray_frames = make_gray(frames)
	
	n_parts = start_labels.shape[0]
	
	lk_labels=np.zeros((n_frames, n_parts, 1, dim), dtype=np.float32)
	st = np.ones((n_frames, n_parts, 1), dtype=np.uint8)
	err = np.zeros((n_frames, n_parts, 1), dtype=np.float32)
	
	lk_labels[0] = start_labels[:, np.newaxis, :].copy()

	for frame in range(n_frames - 1):
		next_points, st_current, err_current = cv.calcOpticalFlowPyrLK(
			gray_frames[frame], 
			gray_frames[frame + 1], 
			lk_labels[frame], 
			None, 
			**lk_params)
		lk_labels[frame + 1] = next_points
		st[frame + 1] = st_current
		err[frame + 1] = err_current
	lk_labels = lk_labels.squeeze()
	return lk_labels, st, err
		
def label_distance(label1, label2):
	assert label1.shape==label2.shape
	assert len(label1.shape)==2
	return np.linalg.norm(label1-label2)

def labels_distances(labels1, labels2):
	assert labels1.shape == labels2.shape
	n_labels = labels1.shape[0]
	distances = np.zeros((n_labels))
	for i in range(n_labels):
		distances[i] = label_distance(labels1[i], labels2[i])
	return distances









